package com.bristlecone.docprocessor.exceptions;

public class GetSeqException extends Exception {
    public GetSeqException(String message) {
	super(message);
    }

    public GetSeqException(String message, Throwable cause) {
	super(message, cause);
    }

    public GetSeqException(Throwable cause) {
	super(cause);
    }
}
