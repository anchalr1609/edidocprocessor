package com.bristlecone.docprocessor.config;

import java.io.IOException;
import java.util.Optional;
import java.util.Properties;

import com.bristlecone.docprocessor.exceptions.EDISystemStartException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConfigsReader {
    private static Properties prop;

    public ConfigsReader() {
	prop = new Properties();
	loadPropertiesForConfig(prop);
	loadPropertiesForQuery(prop);
    }

    /**
     * @param keyAssociatedWithConfigValue
     * @return configValue
     * @throws EDISystemStartException
     */
    public static String getConfigValue(String keyAssociatedWithConfigValue) {
	String configValue = null;

	Optional<String> propertiesValue = Optional.of(Optional.ofNullable(configValue).orElse("defaultValue"));
	configValue = prop.getProperty(keyAssociatedWithConfigValue);
	if (propertiesValue.isPresent()) {
	    log.info("getConfigValue-the required key {} and the value= {}", keyAssociatedWithConfigValue, configValue);
	} else {
	    log.info("getConfigValue-The value is not present for the required config-key{}!!",
		    keyAssociatedWithConfigValue);
	    System.exit(99);
	}
	return configValue;
    }

    /**
     * This method is used to load the configurations from config.properties file.
     * 
     * @param prop
     */
    private static void loadPropertiesForConfig(Properties prop) {
	try {
	    String configFileName = "config.properties";
	    prop.load(ConfigsReader.class.getClassLoader().getResourceAsStream(configFileName));
	} catch (IOException e) {
	    log.error("loadPropertiesForConfig" + PropertiesConfigReader.errorMessageWhileLoadingConfigs
		    + "config.properties" + e);
	    System.exit(99);
	}
    }

    /**
     * This method is used to load the queries from query.properties file.
     * 
     * @param prop
     */
    private static void loadPropertiesForQuery(Properties prop) {
	try {
	    String queryFileName = "query.properties";
	    prop.load(ConfigsReader.class.getClassLoader().getResourceAsStream(queryFileName));
	} catch (IOException e) {
	    log.error("loadPropertiesForQuery" + PropertiesConfigReader.errorMessageWhileLoadingConfigs
		    + "config.properties" + e);
	    System.exit(99);
	}
    }
}
