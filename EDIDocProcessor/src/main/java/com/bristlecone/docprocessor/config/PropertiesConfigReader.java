package com.bristlecone.docprocessor.config;

/**
 * The PropertiesConfigReader class retrieves the values mapped to the keys
 * defined in config.properties file.
 * 
 * @author Anchal.Rajput
 */
public class PropertiesConfigReader {

    public static final String docProcessorDbUserName = ConfigsReader.getConfigValue("DOCPROCESSOR_DB_USERNAME");
    public static final String docProcessorDbPassword = ConfigsReader.getConfigValue("DOCPROCESSOR_DB_PASSWORD");
    public static final String docProcessorPort = ConfigsReader.getConfigValue("DOCPROCESSOR_PORT");
    public static final String docProcessorDriver = ConfigsReader.getConfigValue("DOCPROCESSOR_DB_DRIVER");
    public static final String docProcessorDbUrl = ConfigsReader.getConfigValue("DOCPROCESSOR_DB_URL");
    public static final String ediDocFilesPath = ConfigsReader.getConfigValue("EDIDOC_FILES_PATH");
    public static final String ediDocProcessedFilesPath = ConfigsReader.getConfigValue("EDIDOC_PROCESSED_FILES");
    public static final String ediDocFailedFilesPath = ConfigsReader.getConfigValue("EDIDOC_FAILED_FILES");
    public static final String errorMsgWhileInserting = ConfigsReader.getConfigValue("ERROR_MESSAGE_WHILE_INSERTING");
    public static final String erorMessageWhileGettingSeq = ConfigsReader
	    .getConfigValue("READ_ERROR_MESSAGE_WHILE_GETTING_SEQ");
    public static final String errorMessageWhileUpdating = ConfigsReader.getConfigValue("ERROR_MESSAGE_WHILE_UPDATING");
    public static final String errorMessageWhileReadingFile = ConfigsReader
	    .getConfigValue("ERROR_MESSAGE_WHILE_READING_FILE");
    public static final String errorMessageWhileLoadingConfigs = ConfigsReader
	    .getConfigValue("ERROR_MESSAGE_WHILE_LOADING_CONFIGS");

}
