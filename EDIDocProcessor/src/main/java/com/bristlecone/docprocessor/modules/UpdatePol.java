package com.bristlecone.docprocessor.modules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.bristlecone.docprocessor.app.EDILineUpdateElement;
import com.bristlecone.docprocessor.config.PropertiesConfigReader;
import com.bristlecone.docprocessor.config.QueryConfigReader;
import com.bristlecone.docprocessor.exceptions.UpdateDocumentException;
import com.bristlecone.docprocessor.util.DBUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * This class is used to update into purchase order line by fetching purchase
 * order uid on joining it to product table using primary key and update in pol
 * table and update ship status code and asndocparcelid.
 */
@Slf4j
class UpdatePol {

    private static final int COMPLETE_SHIP_STATUS = 3;
    private static final int PARTIAL_SHIP_STATUS = 2;
    private static final int INITIAL_SHIP_STATUS = 1;

    /**
     * @param ediUpdate object of EDIUpdateElement class
     * @param connObj   Connection object
     * @throws UpdateDocumentException
     */

    public static void updateIntoPurchaseOrderLine(EDILineUpdateElement polUpdate, Connection connObj)
	    throws UpdateDocumentException {
	try {
	    ArrayList<Integer> polItems = getPoLineDetails(polUpdate.getTradingBuyer(), polUpdate.getTradingSeller(),
		    polUpdate.getProductUpc(), polUpdate.getPurchaseOrderNumber(), connObj);

	    int shipStatusCode = getShipStatusCode(polItems.get(1), polUpdate.getShippedQty());

	    UpdatePol.updatePoLineExecute(shipStatusCode, polUpdate.getNextShipLineUid(), polUpdate.getShippedQty(),
		    polUpdate.getAsnDocParcelId(), polItems.get(0), connObj);
	} catch (UpdateDocumentException ue) {
	    log.error("updateIntoPurchaseOrderLine" + PropertiesConfigReader.errorMessageWhileUpdating + " document 850"
		    + ue);
	}
    }

    /**
     * @param tradingBuyer        Retailer Unique ID
     * @param tradingSeller       Supplier Unique ID
     * @param productUpc          ProductUPC code
     * @param purchaseOrderNumber Unique key to identify the dc4parcelids
     * @param connObj             Connection object
     * @return polItems Array list
     * @throws UpdateDocumentException
     */
    private static ArrayList<Integer> getPoLineDetails(int tradingBuyer, int tradingSeller, String productUpc,
	    String purchaseOrderNumber, Connection connObj) throws UpdateDocumentException {
	ResultSet rsObj = null;
	PreparedStatement stmt = null;
	ArrayList<Integer> polItems = new ArrayList<Integer>();

	try {
	    stmt = connObj.prepareStatement(QueryConfigReader.selectIntoPoLine);
	    stmt.setInt(1, tradingBuyer);
	    stmt.setInt(2, tradingSeller);
	    stmt.setString(3, productUpc);
	    stmt.setString(4, purchaseOrderNumber);
	    rsObj = stmt.executeQuery();
	    if (rsObj.next()) {
		polItems.add(rsObj.getInt(1));
		polItems.add(rsObj.getInt(2));
	    }

	} catch (SQLException e) {
	    UpdateDocumentException ie = new UpdateDocumentException(
		    PropertiesConfigReader.errorMessageWhileUpdating + " " + "poline table", e);
	    throw ie;
	} finally {
	    DBUtil.closeStatement(stmt);
	    DBUtil.closeResultSet(rsObj);
	}
	return polItems;
    }

    /**
     * @param currentOrderQty Unique key to identify the dc4parcelids
     * @param shippedQty      NoofShipQty received for each line item
     * @return ship status code this would get updated while processing the ASN
     *         parcel based on line item status
     */
    private static int getShipStatusCode(int currentOrderQty, int shippedQty) {

	if (shippedQty >= currentOrderQty) {
	    log.debug("getShipStatusCode- COMPLETE_SHIP_STATUS");
	    return COMPLETE_SHIP_STATUS;
	} else if (shippedQty < currentOrderQty && shippedQty > 0) {
	    log.debug("getShipStatusCode- PARTIAL_SHIP_STATUS");
	    return PARTIAL_SHIP_STATUS;
	}
	log.debug("getShipStatusCode- INITIAL_SHIP_STATUS");
	return INITIAL_SHIP_STATUS;
    }

    /**
     * @param shipStatusCode   this would get updated while processing the ASN
     *                         parcel based on line item status
     * @param nextShipSequence sequence of shipment lien table
     * @param shippedQty       NoofShipQty received for each line item
     * 
     * @param asnDocParcelId   unique parcel Id
     * @param polineId         purchase order line primary key
     * @param connObj          Connection object
     * @throws UpdateDocumentException
     */
    private static void updatePoLineExecute(int shipStatusCode, int nextShipSequence, int shippedQty,
	    int asnDocParcelId, int polineId, Connection connObj) throws UpdateDocumentException {
	PreparedStatement polStmt = null;

	try {
	    polStmt = connObj.prepareStatement(QueryConfigReader.updateIntoPoLine);
	    polStmt.setInt(1, shipStatusCode);
	    polStmt.setInt(2, nextShipSequence);
	    polStmt.setInt(3, shippedQty);
	    polStmt.setInt(4, asnDocParcelId);
	    polStmt.setInt(5, polineId);
	    polStmt.execute();
	} catch (SQLException e) {
	    UpdateDocumentException up = new UpdateDocumentException(
		    PropertiesConfigReader.errorMessageWhileUpdating + " " + "poline table", e);
	    throw up;
	} finally {
	    DBUtil.closeStatement(polStmt);
	}
    }
}
