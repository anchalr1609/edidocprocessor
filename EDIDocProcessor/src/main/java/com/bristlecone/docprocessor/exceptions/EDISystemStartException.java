package com.bristlecone.docprocessor.exceptions;

public class EDISystemStartException extends Exception {
    public EDISystemStartException(String message) {
	super(message);
    }

    public EDISystemStartException(String message, Throwable cause) {
	super(message, cause);
    }

    public EDISystemStartException(Throwable cause) {

	super(cause);

    }
}
