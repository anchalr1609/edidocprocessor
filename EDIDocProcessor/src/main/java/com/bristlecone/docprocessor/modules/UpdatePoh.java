package com.bristlecone.docprocessor.modules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.bristlecone.docprocessor.app.EDIHeaderUpdateElement;
import com.bristlecone.docprocessor.config.PropertiesConfigReader;
import com.bristlecone.docprocessor.config.QueryConfigReader;
import com.bristlecone.docprocessor.exceptions.UpdateDocumentException;
import com.bristlecone.docprocessor.util.DBUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * This class is used to update into purchase order header by fetching purchase
 * order id on joining it to purchase order line table using primary key and
 * update in purchase order line table and update ship status code.
 *
 */
@Slf4j
public class UpdatePoh {
    public static void updateIntoPurchaseOrderHeader(EDIHeaderUpdateElement pohUpdate, Connection connObj) {
	try {
	    ArrayList<Integer> pohItems = getPoHeaderDetails(pohUpdate.getTradingBuyer(), pohUpdate.getTradingSeller(),
		    pohUpdate.getPurchaseOrderNumber(), connObj);
	    UpdatePoh.updatePoHeaderExecute(pohUpdate.getAsnDocParcelId(), pohItems.get(0), connObj);

	} catch (UpdateDocumentException ue) {
	    log.error("updateIntoPurchaseOrderHeader" + PropertiesConfigReader.errorMsgWhileInserting
		    + "the document 850" + ue);
	}

    }

    /**
     * @param tradingBuyer        Retailer Unique ID
     * @param tradingSeller       Supplier Unique ID
     * @param purchaseOrderNumber Unique key to identify the dc4parcelids
     * @param connObj             Connection object
     * @return pohItems Array list
     * @throws UpdateDocumentException
     */
    private static ArrayList<Integer> getPoHeaderDetails(int tradingBuyer, int tradingSeller,
	    String purchaseOrderNumber, Connection connObj) throws UpdateDocumentException {
	ResultSet rsObj = null;
	PreparedStatement stmt = null;
	ArrayList<Integer> pohItems = new ArrayList<Integer>();

	try {
	    stmt = connObj.prepareStatement(QueryConfigReader.selectIntoPoHeader);
	    stmt.setInt(1, tradingBuyer);
	    stmt.setInt(2, tradingSeller);
	    stmt.setString(3, purchaseOrderNumber);
	    rsObj = stmt.executeQuery();
	    if (rsObj.next()) {
		pohItems.add(rsObj.getInt(1));
	    }
	} catch (SQLException e) {
	    UpdateDocumentException ie = new UpdateDocumentException(
		    PropertiesConfigReader.errorMessageWhileUpdating + " " + "poheader table" + e);
	    throw ie;
	} finally {
	    DBUtil.closeStatement(stmt);
	    DBUtil.closeResultSet(rsObj);
	}
	return pohItems;
    }

    /**
     * @param asnDocParcelId   unique parcel Id
     * @param purchaseOrderUid purchase order header line primary key
     * @param connObj          Connection object
     * @throws UpdateDocumentException
     */
    private static void updatePoHeaderExecute(int asnDocParcelId, int purchaseOrderUid, Connection connObj)
	    throws UpdateDocumentException {
	PreparedStatement pohStmt = null;

	try {
	    pohStmt = connObj.prepareStatement(QueryConfigReader.updateIntoPoHeader);
	    pohStmt.setInt(1, asnDocParcelId);
	    pohStmt.setInt(2, purchaseOrderUid);
	    pohStmt.execute();
	} catch (SQLException e) {
	    UpdateDocumentException up = new UpdateDocumentException(
		    PropertiesConfigReader.errorMessageWhileUpdating + " " + " poheader table" + e);
	    throw up;
	} finally {
	    DBUtil.closeStatement(pohStmt);
	}
    }
}
