package com.bristlecone.docprocessor.app;

import lombok.Getter;
import lombok.Setter;

/**
 * This class provides getter and setter methods to the parameters of purchase
 * order header table using lombok library.
 *
 */
@Getter
@Setter
public class EDIHeaderUpdateElement {
    private int asnDocParcelId;
    private int tradingBuyer;
    private int tradingSeller;
    private String purchaseOrderNumber;
}
