package com.bristlecone.docprocessor.dbconn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.bristlecone.docprocessor.config.PropertiesConfigReader;
import com.bristlecone.docprocessor.exceptions.EDISystemStartException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DBConnection {
    Connection con = null;
    private static DBConnection dbIsntance;

    private DBConnection() {
    }

    public static DBConnection getInstance() {
	if (dbIsntance == null) {
	    dbIsntance = new DBConnection();
	}
	return dbIsntance;
    }

    public Connection getConnection() throws EDISystemStartException {

	try {
	    Class.forName(PropertiesConfigReader.docProcessorDriver);
	    con = DriverManager.getConnection(PropertiesConfigReader.docProcessorDbUrl,
		    PropertiesConfigReader.docProcessorDbUserName, PropertiesConfigReader.docProcessorDbPassword);
	    con.setAutoCommit(false);

	} catch (SQLException ex) {
	    log.error("got error while executing DBConnection" + ex);
	    throw new EDISystemStartException("got error while executing DBConnection", ex);

	} catch (ClassNotFoundException e) {
	    log.error("got error while executing DBConnection" + e);
	    throw new EDISystemStartException("got error while loading the class DBConnection", e);
	}
	return con;
    }

}
