package com.bristlecone.docprocessor.modules;

import com.bristlecone.docprocessor.EDIProcessDoCType;

/**
 * This class is used to get object of concrete classes document 850 and
 * document 856 by passing information as doc type
 *
 */
public class ProcessFactory {
    public EDIProcessDoCType getProcess(String docType) {
	if (docType == null) {
	    return null;
	}
	if (docType.equalsIgnoreCase("850")) {
	    return new Document850();

	} else if (docType.equalsIgnoreCase("856")) {
	    return new Document856();
	}
	return null;
    }
}
