package com.bristlecone.docprocessor.modules;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bristlecone.docprocessor.EDIProcessDoCType;
import com.bristlecone.docprocessor.config.PropertiesConfigReader;
import com.bristlecone.docprocessor.config.QueryConfigReader;
import com.bristlecone.docprocessor.exceptions.EDIDocumentException;
import com.bristlecone.docprocessor.exceptions.EDISystemStartException;
import com.bristlecone.docprocessor.exceptions.GetSeqException;
import com.bristlecone.docprocessor.exceptions.InsertDocumentException;
import com.bristlecone.docprocessor.util.DBUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * This class is used to create method which process the 850 document by
 * inserting into the required tables purchase order header, purchase order
 * line, product table
 *
 */
@Slf4j
public class Document850 implements EDIProcessDoCType {
    private Connection connObj;
    Connection con = null;

    public void processEDIDoc(String fileName, Connection connObj)
	    throws IOException, InsertDocumentException, EDISystemStartException, GetSeqException {
	PreparedStatement polStmt = null;
	PreparedStatement prodStmt = null;
	this.connObj = connObj;
	int shipStatusCode = 1;
	int currentOrderQty = 0;
	int shippedQty = 0;
	int tradingBuyer = 0;
	int tradingSeller = 0;
	int numberOfLineItem;
	int parcelId = 0;
	int asnDocId = 0;
	int purchaseOrderUid = 0;
	String productNumber = null;
	String productUpc = null;
	String purchaseOrderNumber = null;

	try {
	    String[] parametersOf850 = null;
	    String splitPattern = "\\|";
	    String data = new String(Files.readAllBytes(Paths.get("EDIDoc_files/" + fileName)));
	    String[] arrOfParcel = data.split(splitPattern);
	    polStmt = connObj.prepareStatement(QueryConfigReader.insertIntoPoLine);
	    prodStmt = connObj.prepareStatement(QueryConfigReader.insertIntoProduct);

	    if (arrOfParcel[0] != null) {
		parametersOf850 = arrOfParcel[0].split(",");
		numberOfLineItem = arrOfParcel.length - 2;
		parcelId = Integer.parseInt(parametersOf850[0]);
		tradingBuyer = Integer.parseInt(parametersOf850[2]);
		tradingSeller = Integer.parseInt(parametersOf850[3]);
		purchaseOrderNumber = parametersOf850[1];
		purchaseOrderUid = getpoHeaderSeq(connObj);

		insertIntoPurchaseOrderHeader(parcelId, purchaseOrderNumber, tradingBuyer, tradingSeller,
			shipStatusCode, numberOfLineItem, asnDocId, purchaseOrderUid);
	    }

	    for (int iterateNoOfLineItem = 1; iterateNoOfLineItem <= arrOfParcel.length - 1; iterateNoOfLineItem++) {
		String[] linesOfparametersOf850 = arrOfParcel[iterateNoOfLineItem].split(",");
		currentOrderQty = Integer.parseInt(linesOfparametersOf850[3]);
		int purchaseOrderLineUid = getpoLineSeq(connObj);
		int productUidBuyer = getproductIDSeq(connObj);
		polStmt = insertIntoPurchaseOrderLine(parcelId, purchaseOrderNumber, tradingBuyer, tradingSeller,
			shipStatusCode, currentOrderQty, shippedQty, asnDocId, purchaseOrderLineUid, purchaseOrderUid,
			productUidBuyer, polStmt);
		productUpc = linesOfparametersOf850[2];
		prodStmt = insertIntoProduct(productNumber, productUpc, linesOfparametersOf850[1], tradingBuyer,
			tradingSeller, linesOfparametersOf850[0], productUidBuyer, prodStmt);

	    }
	    polStmt.executeBatch();
	    prodStmt.executeBatch();

	} catch (InsertDocumentException ie) {
	    DBUtil.rollBack(connObj);
	    String errorMessage = ie.getMessage();
	    EDIDocumentException ediDoc = new EDIDocumentException();

	    ediDoc.setDocParcelId(parcelId);
	    ediDoc.setTradingPartnerUidBuyer(tradingBuyer);
	    ediDoc.setTradingPartnerUidSeller(tradingSeller);
	    ediDoc.setDocumentType("850");
	    ediDoc.setErrorMessage(errorMessage);

	    DocError.insertIntoDocError(ediDoc);
	    throw ie;
	} catch (Exception ne) {
	    InsertDocumentException ie = new InsertDocumentException(
		    PropertiesConfigReader.errorMsgWhileInserting + " " + "the tables of doc 850" + ne);
	    throw ie;
	} finally {
	    DBUtil.closeStatement(polStmt);
	    DBUtil.closeStatement(prodStmt);
	    DBUtil.commit(connObj);
	}
    }

    /**
     * @param purchaseOrderUid        Primary Key and Unique 11 key Sequence Digit
     * @param docParcelId             Unique key to for each separate parcels
     * @param factCreatedDate         Sysdate when doc is getting inserted into the
     *                                database
     * @param purchaseordernumber     Unique key to identify the dc4parcelids
     * @param tradingPartnerUidBuyer  Retailer Unique ID
     * @param tradingPartnerUidSeller Supplier Unique ID
     * @param shipStatusCode          this file would get updated while processing
     *                                the ASN parcel based on line item status
     * @param lineItemsOrdered        Total Number of Line Item for a single
     *                                purchase order
     * @throws SQLException
     * @throws EDISystemStartException
     * @throws InsertDocumentException
     */
    private void insertIntoPurchaseOrderHeader(int docParcelId, String purchaseOrderNumber, int tradingPartnerUidBuyer,
	    int tradingPartnerUidSeller, int shipStatusCode, int lineItemsOrdered, int asnDocParcelId,
	    int purchaseOrderUid) throws InsertDocumentException {
	PreparedStatement pohStmtObj = null;

	try {
	    pohStmtObj = connObj.prepareStatement(QueryConfigReader.insertIntoPoHeader);
	    pohStmtObj.setInt(1, purchaseOrderUid);
	    pohStmtObj.setInt(2, docParcelId);
	    pohStmtObj.setString(3, purchaseOrderNumber);
	    pohStmtObj.setInt(4, tradingPartnerUidBuyer);
	    pohStmtObj.setInt(5, tradingPartnerUidSeller);
	    pohStmtObj.setInt(6, shipStatusCode);
	    pohStmtObj.setInt(7, lineItemsOrdered);
	    pohStmtObj.setInt(8, asnDocParcelId);
	    pohStmtObj.execute();
	} catch (SQLException e) {
	    InsertDocumentException ie = new InsertDocumentException(
		    PropertiesConfigReader.errorMsgWhileInserting + " " + "PurchaseOrderHeader table" + e);
	    throw ie;
	} finally {
	    DBUtil.closeStatement(pohStmtObj);
	}
    }

    /**
     * @param docParcelId             Unique key to for each separate parcels
     * @param factCreatedDate         Sys date when doc is getting inserted into the
     *                                database
     * @param purchaseOrderUid        Foreign Key
     * @param purchaseordernumber     Unique key to identify the dc4parcelids
     * @param tradingPartnerUidBuyer  Retailer Unique ID
     * @param tradingPartnerUidSeller Supplier Unique ID
     * @param shipStatusCode          this file would get updated while processing
     *                                the ASN parcel based on line item status
     * @param currentorderqty         NoofOrder received for each line item
     * @param shippedQty              NoofShipQty received for each line item
     * @return Statement object
     *
     * @throws SQLException
     * @throws EDISystemStartException
     * @throws InsertDocumentException
     */
    private PreparedStatement insertIntoPurchaseOrderLine(int docParcelId, String purchaseOrderNumber,
	    int tradingPartnerUidBuyer, int tradingPartnerUidSeller, int shipStatusCode, int currentOrderQty,
	    int shippedQty, int asnDocParcelId, int purchaseOrderLineuid, int purchaseOrderUid, int productUidBuyer,
	    PreparedStatement polStmtObj) throws InsertDocumentException {

	try {
	    polStmtObj.setInt(1, purchaseOrderLineuid);
	    polStmtObj.setInt(2, docParcelId);
	    polStmtObj.setString(3, purchaseOrderNumber);
	    polStmtObj.setInt(4, tradingPartnerUidBuyer);
	    polStmtObj.setInt(5, tradingPartnerUidSeller);
	    polStmtObj.setInt(6, shipStatusCode);
	    polStmtObj.setInt(7, currentOrderQty);
	    polStmtObj.setInt(8, shippedQty);
	    polStmtObj.setInt(9, asnDocParcelId);
	    polStmtObj.setInt(10, purchaseOrderUid);
	    polStmtObj.setInt(11, productUidBuyer);
	    polStmtObj.addBatch();
	} catch (SQLException e) {
	    InsertDocumentException ie = new InsertDocumentException(
		    PropertiesConfigReader.errorMsgWhileInserting + " " + "PurchaseOrderLine table" + e);
	    throw ie;
	}
	return polStmtObj;
    }

    /**
     * @param productNumber           Received product number from parcels
     * @param productUpc              ProductUPC code
     * @param productDescription      Description of product
     * @param tradingPartnerUidBuyer  Retailer Unique ID
     * @param tradingPartnerUidSeller Supplier Unique ID
     * @param productName             Name of the product
     * @return Statement object
     * @throws SQLException
     * @throws EDISystemStartException
     * @throws InsertDocumentException
     */
    private PreparedStatement insertIntoProduct(String productNumber, String productUpc, String productDescription,
	    int tradingPartnerUidBuyer, int tradingPartnerUidSeller, String productName, int productUidBuyer,
	    PreparedStatement prodStmtObj) throws InsertDocumentException {

	try {
	    prodStmtObj.setInt(1, productUidBuyer);
	    prodStmtObj.setString(2, productNumber);
	    prodStmtObj.setString(3, productUpc);
	    prodStmtObj.setString(4, productDescription);
	    prodStmtObj.setInt(5, tradingPartnerUidBuyer);
	    prodStmtObj.setInt(6, tradingPartnerUidSeller);
	    prodStmtObj.setString(7, productName);
	    prodStmtObj.addBatch();
	} catch (SQLException e) {
	    InsertDocumentException ie = new InsertDocumentException(
		    PropertiesConfigReader.errorMsgWhileInserting + " " + "Product table" + e);
	    throw ie;
	}
	return prodStmtObj;
    }

    private static int getpoLineSeq(Connection connObj) throws GetSeqException {
	PreparedStatement polSeqObj = null;
	ResultSet rslObj = null;
	int nextSeq = 0;

	try {
	    polSeqObj = connObj.prepareStatement("select seqForPol.nextval from DUAL");
	    rslObj = polSeqObj.executeQuery();
	    if (rslObj.next()) {
		nextSeq = rslObj.getInt(1);
	    }
	} catch (SQLException e) {
	    GetSeqException se = new GetSeqException(
		    PropertiesConfigReader.erorMessageWhileGettingSeq + " " + "poline table" + e);
	    throw se;
	} finally {
	    DBUtil.closeStatement(polSeqObj);
	    DBUtil.closeResultSet(rslObj);
	}
	return nextSeq;
    }

    private static int getpoHeaderSeq(Connection connObj) throws GetSeqException {
	PreparedStatement pohSequenceObj = null;
	ResultSet rslObj = null;
	int nextSeq = 0;

	try {
	    pohSequenceObj = connObj.prepareStatement("select seqForPoheader.nextval from DUAL");
	    rslObj = pohSequenceObj.executeQuery();
	    if (rslObj.next()) {
		nextSeq = rslObj.getInt(1);
	    }
	} catch (SQLException e) {
	    GetSeqException se = new GetSeqException(
		    PropertiesConfigReader.erorMessageWhileGettingSeq + " " + "poheader table" + e);
	    throw se;
	} finally {
	    DBUtil.closeStatement(pohSequenceObj);
	    DBUtil.closeResultSet(rslObj);
	}
	return nextSeq;
    }

    private static int getproductIDSeq(Connection connObj) throws GetSeqException {
	PreparedStatement productSequenceObj = null;
	ResultSet rslObj = null;
	int nextSeq = 0;

	try {
	    productSequenceObj = connObj.prepareStatement("select seqforproduct.nextval from DUAL");
	    rslObj = productSequenceObj.executeQuery();
	    if (rslObj.next()) {
		nextSeq = rslObj.getInt(1);
	    }
	} catch (SQLException e) {
	    GetSeqException se = new GetSeqException(
		    PropertiesConfigReader.erorMessageWhileGettingSeq + " " + "of product table" + e);
	    throw se;
	} finally {
	    DBUtil.closeStatement(productSequenceObj);
	    DBUtil.closeResultSet(rslObj);
	}
	return nextSeq;
    }
}
