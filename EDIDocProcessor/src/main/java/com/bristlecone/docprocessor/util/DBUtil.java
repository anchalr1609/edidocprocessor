package com.bristlecone.docprocessor.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;

/**
 * This class is used to create template methods for repeated try catch, we have
 * used these methods everywhere it is required.
 *
 */
@Slf4j
public class DBUtil {

    public static void closeConnection(Connection connObj) {
	try {
	    Optional<Connection> connOpt = Optional.ofNullable(connObj);
	    if (connOpt.isPresent())
		connObj.close();
	} catch (SQLException ex) {
	    log.error("Cannot close the connection!!" + ex);
	}
    }

    public static void closeStatement(PreparedStatement stmt) {
	try {
	    Optional<PreparedStatement> stmtOpt = Optional.ofNullable(stmt);
	    if (stmtOpt.isPresent())
		stmt.close();
	} catch (SQLException ex) {
	    log.error("cannot close the statement!!" + ex);

	}
    }

    public static void commit(Connection connObj) {
	try {
	    Optional<Connection> connOpt = Optional.ofNullable(connObj);
	    if (connOpt.isPresent())
		connObj.commit();
	} catch (SQLException ex) {
	    log.error("Cannot commit the connection!!" + ex);
	}
    }

    public static void rollBack(Connection connObj) {
	try {
	    Optional<Connection> connOpt = Optional.ofNullable(connObj);
	    if (connOpt.isPresent())
		connObj.rollback();
	} catch (SQLException ex) {
	    log.error("Cannot rollback the connection!!" + ex);
	}
    }

    public static void closeResultSet(ResultSet resObj) {
	try {
	    Optional<ResultSet> connOpt = Optional.ofNullable(resObj);
	    if (connOpt.isPresent())
		resObj.close();
	} catch (Exception ex) {
	    log.error("Cannot close the connection!!" + ex);
	}
    }
}
