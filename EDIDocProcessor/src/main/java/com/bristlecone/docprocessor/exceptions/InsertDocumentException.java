package com.bristlecone.docprocessor.exceptions;

public class InsertDocumentException extends Exception {
    public InsertDocumentException(String message) {
	super(message);
    }

    public InsertDocumentException(String message, Throwable cause) {
	super(message, cause);
    }

    public InsertDocumentException(Throwable cause) {
	super(cause);
    }
}