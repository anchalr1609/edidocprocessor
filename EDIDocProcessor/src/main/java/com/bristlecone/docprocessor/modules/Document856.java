package com.bristlecone.docprocessor.modules;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bristlecone.docprocessor.EDIProcessDoCType;
import com.bristlecone.docprocessor.app.EDIHeaderUpdateElement;
import com.bristlecone.docprocessor.app.EDILineUpdateElement;
import com.bristlecone.docprocessor.config.PropertiesConfigReader;
import com.bristlecone.docprocessor.config.QueryConfigReader;
import com.bristlecone.docprocessor.exceptions.EDISystemStartException;
import com.bristlecone.docprocessor.exceptions.GetSeqException;
import com.bristlecone.docprocessor.exceptions.InsertDocumentException;
import com.bristlecone.docprocessor.util.DBUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * This class is used to create method which process the 850 document by
 * inserting into the required tables shipment header, shipment line.
 *
 */
@Slf4j
public class Document856 implements EDIProcessDoCType {
    private Connection connObj;
    Connection con = null;

    public void processEDIDoc(String fileName, Connection connObj)
	    throws IOException, InsertDocumentException, GetSeqException {
	PreparedStatement shipStmt = null;
	this.connObj = connObj;
	int shippedQty = 0;
	int tradingBuyer = 0;
	int tradingSeller = 0;
	int parcelId = 0;
	int shimentuid = 0;
	int currentAmountInvoiced = 0;
	String shipmentNumber = null;
	String purchaseOrderNumber = null;

	try {
	    int totalOrders = 0;
	    String[] parametersOf856 = null;
	    String splitPattern = "\\|";
	    String data = new String(Files.readAllBytes(Paths.get("EDIDoc_files/" + fileName)));
	    String[] arrOfParcel = data.split(splitPattern);
	    shipStmt = connObj.prepareStatement(QueryConfigReader.insertIntoShLine);

	    if (arrOfParcel[0] != null) {
		parametersOf856 = arrOfParcel[0].split(",");
		parcelId = Integer.parseInt(parametersOf856[0]);
		tradingBuyer = Integer.parseInt(parametersOf856[2]);
		tradingSeller = Integer.parseInt(parametersOf856[3]);
		shipmentNumber = parametersOf856[4];
		purchaseOrderNumber = parametersOf856[1];
		shimentuid = getShipmentHeaderSeq(connObj);

		insertIntoShipmentHeader(parcelId, shipmentNumber, tradingBuyer, tradingSeller, totalOrders,
			purchaseOrderNumber, shimentuid);
	    }
	    for (int iterateNoOfLineItem = 1; iterateNoOfLineItem <= arrOfParcel.length - 1; iterateNoOfLineItem++) {
		int nextShipSequence = getShipmentLineSeq(connObj);
		EDILineUpdateElement polUpdate = new EDILineUpdateElement();
		EDIHeaderUpdateElement pohUpdate = new EDIHeaderUpdateElement();
		String[] lineParametersOf856 = arrOfParcel[1].split(",");
		shippedQty = Integer.parseInt(lineParametersOf856[3]);
		String productUpc = lineParametersOf856[2];

		shipStmt = insertIntoShipmentLine(parcelId, shipmentNumber, tradingBuyer, tradingSeller, productUpc,
			currentAmountInvoiced, purchaseOrderNumber, nextShipSequence, shimentuid, shipStmt);

		pohUpdate.setTradingBuyer(tradingBuyer);
		pohUpdate.setTradingSeller(tradingSeller);
		pohUpdate.setPurchaseOrderNumber(purchaseOrderNumber);
		pohUpdate.setAsnDocParcelId(parcelId);

		UpdatePoh.updateIntoPurchaseOrderHeader(pohUpdate, connObj);

		polUpdate.setProductUpc(productUpc);
		polUpdate.setPurchaseOrderNumber(purchaseOrderNumber);
		polUpdate.setTradingBuyer(tradingBuyer);
		polUpdate.setTradingSeller(tradingSeller);
		polUpdate.setShippedQty(shippedQty);
		polUpdate.setAsnDocParcelId(parcelId);
		polUpdate.setNextShipLineUid(nextShipSequence);

		UpdatePol.updateIntoPurchaseOrderLine(polUpdate, connObj);
	    }
	    shipStmt.executeBatch();
	} catch (InsertDocumentException ie) {
	    DBUtil.rollBack(connObj);
	    throw ie;
	} catch (Exception ne) {
	    InsertDocumentException ie = new InsertDocumentException(
		    PropertiesConfigReader.errorMsgWhileInserting + " " + "the tables of doc856" + ne);
	    throw ie;
	} finally {
	    DBUtil.closeStatement(shipStmt);
	    DBUtil.commit(connObj);
	}
    }

    private void insertIntoShipmentHeader(int docParcelId, String shipmentNumber, int tradingPartnerUidBuyer,
	    int tradingPartnerUidSeller, int totalOrders, String purchaseOrderNumber, int shimentuid)
	    throws InsertDocumentException {
	PreparedStatement shipStmtObj = null;

	try {
	    shipStmtObj = connObj.prepareStatement(QueryConfigReader.insertIntoShHeader);
	    shipStmtObj.setInt(1, shimentuid);
	    shipStmtObj.setInt(2, docParcelId);
	    shipStmtObj.setString(3, shipmentNumber);
	    shipStmtObj.setInt(4, tradingPartnerUidBuyer);
	    shipStmtObj.setInt(5, tradingPartnerUidSeller);
	    shipStmtObj.setInt(6, totalOrders);
	    shipStmtObj.setString(7, purchaseOrderNumber);
	    shipStmtObj.execute();
	} catch (SQLException e) {
	    InsertDocumentException ie = new InsertDocumentException(
		    PropertiesConfigReader.errorMsgWhileInserting + " " + "shipmentheader table" + e);
	    throw ie;
	} finally {
	    DBUtil.closeStatement(shipStmtObj);
	}
    }

    private PreparedStatement insertIntoShipmentLine(int docParcelId, String shipmentNumber, int tradingPartnerUidBuyer,
	    int tradingPartnerUidSeller, String productUpc, int currentAmountInvoiced, String purchaseOrderNumber,
	    int nextShipSequence, int shimentuid, PreparedStatement shiplinestmtObj)
	    throws InsertDocumentException, EDISystemStartException {

	try {
	    shiplinestmtObj.setInt(1, nextShipSequence);
	    shiplinestmtObj.setInt(2, docParcelId);
	    shiplinestmtObj.setString(3, shipmentNumber);
	    shiplinestmtObj.setInt(4, tradingPartnerUidBuyer);
	    shiplinestmtObj.setInt(5, tradingPartnerUidSeller);
	    shiplinestmtObj.setString(6, productUpc);
	    shiplinestmtObj.setInt(7, currentAmountInvoiced);
	    shiplinestmtObj.setString(8, purchaseOrderNumber);
	    shiplinestmtObj.setInt(9, shimentuid);
	    shiplinestmtObj.addBatch();
	} catch (SQLException e) {
	    InsertDocumentException ie = new InsertDocumentException(
		    PropertiesConfigReader.errorMsgWhileInserting + " " + "shipmentline table" + e);
	    throw ie;
	}
	return shiplinestmtObj;
    }

    private static int getShipmentLineSeq(Connection connObj) throws GetSeqException {
	PreparedStatement shipObj = null;
	ResultSet rslObj = null;
	int nextSeq = 0;

	try {
	    shipObj = connObj.prepareStatement("select seqForShl.nextval from DUAL");
	    rslObj = shipObj.executeQuery();
	    if (rslObj.next()) {
		nextSeq = rslObj.getInt(1);
	    }

	} catch (SQLException e) {
	    GetSeqException se = new GetSeqException(
		    PropertiesConfigReader.erorMessageWhileGettingSeq + " " + "shipment line table" + e);
	    throw se;
	} finally {
	    DBUtil.closeStatement(shipObj);
	    DBUtil.closeResultSet(rslObj);
	}
	return nextSeq;
    }

    private static int getShipmentHeaderSeq(Connection connObj) throws GetSeqException {
	PreparedStatement shipHeaderObj = null;
	ResultSet rslObj = null;
	int nextSeq = 0;

	try {
	    shipHeaderObj = connObj.prepareStatement("select seqForShHeader.nextval from DUAL");
	    rslObj = shipHeaderObj.executeQuery();
	    if (rslObj.next()) {
		nextSeq = rslObj.getInt(1);
	    }
	} catch (SQLException e) {
	    GetSeqException se = new GetSeqException(
		    PropertiesConfigReader.erorMessageWhileGettingSeq + " " + "shipment header table" + e);
	    throw se;
	} finally {
	    DBUtil.closeStatement(shipHeaderObj);
	    DBUtil.closeResultSet(rslObj);
	}
	return nextSeq;
    }
}
