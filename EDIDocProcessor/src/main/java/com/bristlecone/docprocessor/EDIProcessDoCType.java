package com.bristlecone.docprocessor;

import java.io.IOException;
import java.sql.Connection;

import com.bristlecone.docprocessor.exceptions.EDISystemStartException;
import com.bristlecone.docprocessor.exceptions.GetSeqException;
import com.bristlecone.docprocessor.exceptions.InsertDocumentException;

public interface EDIProcessDoCType {
    void processEDIDoc(String file, Connection conn)
	    throws IOException, InsertDocumentException, EDISystemStartException, GetSeqException;
}
