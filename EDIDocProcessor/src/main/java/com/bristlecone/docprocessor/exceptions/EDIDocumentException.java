package com.bristlecone.docprocessor.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EDIDocumentException {
    private int tradingPartnerUidBuyer;
    private int tradingPartnerUidSeller;
    private int docParcelId;
    private String documentType;
    private String errorMessage;

}
