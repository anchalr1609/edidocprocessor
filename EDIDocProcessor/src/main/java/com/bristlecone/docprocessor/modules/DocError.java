package com.bristlecone.docprocessor.modules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.bristlecone.docprocessor.config.PropertiesConfigReader;
import com.bristlecone.docprocessor.config.QueryConfigReader;
import com.bristlecone.docprocessor.dbconn.DBConnection;
import com.bristlecone.docprocessor.exceptions.EDIDocumentException;
import com.bristlecone.docprocessor.exceptions.EDISystemStartException;
import com.bristlecone.docprocessor.exceptions.InsertDocumentException;
import com.bristlecone.docprocessor.util.DBUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * This class is used to create method to insert into the Doc Error table which
 * is written once and called from every place whenever any error occurs while
 * processing the document.
 */
@Slf4j
public class DocError {

    public static void insertIntoDocError(EDIDocumentException ediDoc)
	    throws InsertDocumentException, EDISystemStartException {

	DocError.insertIntoDocError(ediDoc.getDocParcelId(), ediDoc.getTradingPartnerUidBuyer(),
		ediDoc.getTradingPartnerUidSeller(), ediDoc.getDocumentType(), ediDoc.getErrorMessage(),
		DBConnection.getInstance().getConnection());
    }

    public static void insertIntoDocError(int docParcelId, int tradingPartnerUidBuyer, int tradingPartnerUidSeller,
	    String documentType, String errorMessage, Connection connObj) throws InsertDocumentException {
	PreparedStatement stmt = null;

	try {
	    stmt = connObj.prepareStatement(QueryConfigReader.insertIntoDocError);
	    stmt.setInt(1, docParcelId);
	    stmt.setInt(2, tradingPartnerUidBuyer);
	    stmt.setInt(3, tradingPartnerUidSeller);
	    stmt.setString(4, documentType);
	    stmt.setString(5, errorMessage);
	    stmt.execute();
	} catch (SQLException e) {
	    System.out.println(e);
	    InsertDocumentException ie = new InsertDocumentException(
		    PropertiesConfigReader.errorMsgWhileInserting + " " + "DocError table" + e);
	    throw ie;
	} finally {
	    DBUtil.commit(connObj);
	    DBUtil.closeConnection(connObj);
	}
    }
}
