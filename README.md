# EDIDOCProcessor
1. This project is to build the processor which will process the exchange documents between Retailer and Suppliers to fullfillment the order.
2. These document may come in below format :
            a. As a text file 
            b. XML document
            c. Via Rest API
            Note : Currently This project is supporting only text file processing.
3. Order Flow : First Retailer sends a purchaseorderrequest to supplier and that document is called as 850 document.
                If supplier is able to fullfill the requirement then it would send the 856(Advance Ship Notice Parcel) document which would consist the amount and respective shipped Quantity.  
                Note : Currently This project is supporting only two type of documents(850 & 856) but lateronwards it can be enhanced.     

### Prerequisites
1. Jdk 8 should be installed