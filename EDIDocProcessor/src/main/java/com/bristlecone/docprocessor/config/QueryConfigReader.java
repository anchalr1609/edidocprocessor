package com.bristlecone.docprocessor.config;

/**
 * The QueryConfigReader class retrieves the values mapped to the keys defined
 * in query.properties file.
 * 
 * @author Anchal.Rajput
 *
 */
public class QueryConfigReader {

    public static final String insertIntoPoHeader = ConfigsReader.getConfigValue("INSERT_EDIDOC_POHEADER_QUERY");
    public static final String insertIntoPoLine = ConfigsReader.getConfigValue("INSERT_EDIDOC_POLINE_QUERY");
    public static final String insertIntoProduct = ConfigsReader.getConfigValue("INSERT_EDIDOC_PRODUCT_QUERY");
    public static final String insertIntoDocError = ConfigsReader.getConfigValue("INSERT_EDIDOC_DOCERROR_QUERY");
    public static final String insertIntoShHeader = ConfigsReader.getConfigValue("INSERT_EDIDOC_SHHEADER_QUERY");
    public static final String insertIntoShLine = ConfigsReader.getConfigValue("INSERT_EDIDOC_SHLINE_QUERY");
    public static final String selectIntoPoLine = ConfigsReader.getConfigValue("SELECT_EDIDOC_POLINE_QUERY");
    public static final String updateIntoPoLine = ConfigsReader.getConfigValue("UPDATE_EDIDOC_POLINE_QUERY");
    public static final String selectIntoPoHeader = ConfigsReader.getConfigValue("SELECT_EDIDOC_POH_QUERY");
    public static final String updateIntoPoHeader = ConfigsReader.getConfigValue("UPDATE_EDIDOC_POH_QUERY");
}
