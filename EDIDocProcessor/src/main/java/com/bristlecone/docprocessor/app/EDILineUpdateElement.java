package com.bristlecone.docprocessor.app;

import lombok.Getter;
import lombok.Setter;

/**
 * This class provides getter and setter methods to the parameters of purchase
 * order line table using lombok library
 */
@Getter
@Setter
public class EDILineUpdateElement {
    private int asnDocParcelId;
    private int shippedQty;
    private int poLineId;
    private int tradingBuyer;
    private int tradingSeller;
    private String purchaseOrderNumber;
    private String productUpc;
    private int nextShipLineUid;
}
