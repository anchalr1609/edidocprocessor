package com.bristlecone.docprocessor.exceptions;

public class UpdateDocumentException extends Exception {
    public UpdateDocumentException(String message) {
	super(message);
    }

    public UpdateDocumentException(String message, Throwable cause) {
	super(message, cause);
    }

    public UpdateDocumentException(Throwable cause) {
	super(cause);
    }

}
