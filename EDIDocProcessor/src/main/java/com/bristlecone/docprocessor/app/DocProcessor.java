package com.bristlecone.docprocessor.app;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import com.bristlecone.docprocessor.EDIProcessDoCType;
import com.bristlecone.docprocessor.config.ConfigsReader;
import com.bristlecone.docprocessor.config.PropertiesConfigReader;
import com.bristlecone.docprocessor.dbconn.DBConnection;
import com.bristlecone.docprocessor.exceptions.EDISystemStartException;
import com.bristlecone.docprocessor.exceptions.GetSeqException;
import com.bristlecone.docprocessor.exceptions.InsertDocumentException;
import com.bristlecone.docprocessor.modules.ProcessFactory;
import com.bristlecone.docprocessor.util.DBUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DocProcessor {

    /**
     * The DocProcessor reads parameters from configuration files, read the file
     * from current directory,process the required document, move the processed
     * folder to the EDIDocprocessed folder and failed processed file to the another
     * folder.
     * 
     * @author Anchal.Rajput
     * @version 1.8
     * @throws InterruptedException
     * @throws InsertDocumentException
     * @throws EDIDoc850Exception
     * @throws IOException
     * @throws SQLException
     */
    public static void main(String args[]) throws InterruptedException {

	Connection connectionObject = null;

	try {
	    new ConfigsReader();
	    connectionObject = DBConnection.getInstance().getConnection();
	} catch (EDISystemStartException edi) {
	    log.error("main-Error occured while starting the process Exiting " + edi);
	    System.exit(99);
	} catch (Exception e) {
	    log.error("main-error occurred while starting and reading the config values and queries");
	    System.exit(99);
	}
	File currentDir = new File(PropertiesConfigReader.ediDocFilesPath);// current directory
	if (currentDir != null) {
	    try {
		processDocument(currentDir, connectionObject);
	    } catch (InsertDocumentException ie) {
		log.error("processDocument-" + PropertiesConfigReader.errorMsgWhileInserting + ie);
		System.exit(99);
	    } catch (EDISystemStartException e) {
		log.error("processDocument-" + PropertiesConfigReader.errorMessageWhileReadingFile + e);
	    }
	}
    }

    /**
     * This method checks whether the file type is a directory type or file type and
     * processes the first document placed in the current directory.
     * 
     * @param dir
     * @param connObj
     * @throws EDISystemStartException
     * @throws InsertDocumentException
     * @throws InterruptedException
     */
    private static void processDocument(final File dir, Connection connObj)
	    throws EDISystemStartException, InsertDocumentException, InterruptedException {
	EDIProcessDoCType edidoc;
	ProcessFactory processfactory;
	processfactory = new ProcessFactory();
	while (true) {
	    String fileName = null;
	    Connection conObj = null;

	    try {
		File[] ediDocFiles = dir.listFiles();
		for (File file : ediDocFiles) {
		    conObj = DBConnection.getInstance().getConnection();

		    if (file != null) {

			if (file.isDirectory()) {
			    processDocument(file, conObj);
			} else {
			    String[] arrOfStr = file.getName().split("_");
			    fileName = file.getName();
			    edidoc = processfactory.getProcess(arrOfStr[0]);
			    edidoc.processEDIDoc(fileName, conObj);
			    moveIntoDocProcessedFolder(fileName);
			}
		    } else {
			Thread.sleep(1000);
		    }
		    DBUtil.closeConnection(conObj);
		}
	    } catch (InsertDocumentException ie) {
		moveIntoDocFailedFolder(fileName);
		throw ie;
	    } catch (IOException e) {
		throw new EDISystemStartException("processDocument-"
			+ PropertiesConfigReader.errorMessageWhileReadingFile + "current directory " + e);
	    } catch (GetSeqException e) {
		log.error("processDocument-" + PropertiesConfigReader.errorMessageWhileReadingFile + e);
	    }
	}
    }

    /**
     * This method moves the successfully processed file to the
     * EDIDocProcessedFolder after deleting the file from current directory.
     * 
     * @param fileName
     * @return
     */
    public static void moveIntoDocProcessedFolder(String fileName) {
	File sourceFile = new File(PropertiesConfigReader.ediDocFilesPath + "/" + fileName);
	File destinationFolder = new File(
		PropertiesConfigReader.ediDocProcessedFilesPath + "/" + fileName + "_Processed");

	if (sourceFile.renameTo(destinationFolder)) {
	    sourceFile.delete();
	    log.info("moveIntoDocProcessedFolder-File moved to EDIDOC_PROCESSED_FILES folder successfully");
	} else {
	    log.info("moveIntoDocProcessedFolder-Failed in moving file to EDIDOC_PROCESSED_FILES folder");
	}
    }

    /**
     * This method moves the unsuccessfully processed file to the
     * EDIDocProcessFailedFolder after deleting the file from current directory.
     * 
     * @param fileName
     */
    private static void moveIntoDocFailedFolder(String fileName) {
	File failedSourceFile = new File(PropertiesConfigReader.ediDocFilesPath + "/" + fileName);

	if (failedSourceFile.renameTo(new File(PropertiesConfigReader.ediDocFailedFilesPath + "_Failed"))) {
	    failedSourceFile.delete();
	    log.info("moveIntoDocFailedFolder-Files moved to EDIDOC_FAILED_FILES folder successfully");
	} else {
	    log.info("moveIntoDocFailedFolder-Failed in moving file to EDIDOC_FAILED_FILES folder");
	}
    }
}
